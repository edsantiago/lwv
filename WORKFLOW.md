Workflow for LWL Videos
=======================

This document describes the workflow Ed uses, as of November 2023.

### Requirements

Linux. Perl. ImageMagick. Probably more, please update list as you find those.

### Get Recordings

Someone records the presentation, then delivers SD card (or link)
to Ed. That's beyond the scope of this document.

### Transfer Recordings

Before anything, mkdir "/path/to/YYYY-MM-DDT18:00:00Z/LWVLA Lunch with a Leader"
(note the T and Z, they are required). cd there.

Copy video files to this directory. If they're from the camera, they will
be DSC\*.MP4 files. If you have multiple files, you need to glom them all
into one. Use `concat`:

    $ ../../scripts/concat DSC01[234].MP4

(Ignore test files; just make sure you look for and skip those).

`concat` will create `zoom.mp4`. It will also try to adjust sound levels.

### Create setup files

Still in that subdirectory, create the following files:

  * description - two to four lines that will be shown on the title page. First line should be presenter's name, 2nd should be title, 3rd (if present) may be part of their title or other info. On 2023-11-16 we had two presenters, so 1st and 3rd lines were names, 2nd and 4th were titles. This is read by titlepage-lwl, which will probably need tweaks to spacing, font sizes, whatever, because every situation is always different.
  * blurb - copy/paste from Karyl Ann's email. This is the bio that goes on Youtube. This is not read by any code.
  * metadata - here is where you'll put start and end times. But not yet.

### Create titlepage

Run `../../scripts/titlepage-lwl`. This creates `titlepage.png` and scales it to the same size as `zoom.mp4`. Use your favorite viewer to look at it. Make sure it looks OK. If it doesn't, tweak the script.

### Get timings

Watch `zoom.mp4`, with OSD on, and get a sense for where you want to start the video. Remember there's about 10 seconds of titlepage, so start a little before you want the person on camera. Write this into `metadata` as follows:

    from = MM:SS

Like, `0:10`, or `4:55` if there's a long intro. Whatever.

*Note*: You can use decimal fractions of seconds. Sometimes you need to.

Skip to near the end, and repeat the process, figuring out where you want to END. Ending is a sharp cutoff, no fancy fade or anything. Write that, too, into `metadata`:

    to = MM:SS  (or sometimes HH:MM:SS)

E.g., `56:20` or `1:06:40`.

I like to add brief legends when new speakers are shown on screen. For each of those, find the timing, and add a `subtitle` line to the file:

    subtitle = 0:14 Jill Dixon, Deputy Directory, Food Depot

Each comma will be a newline in the subtitle, so the above will show on three lines.

Add as many of these as you want.

### Test timings

Run `../../scripts/truncate-video --test`. With `--test` you get two files, `youtube-begin.mp4` and `youtube-end.mp4`, and it only takes a minute or two (unlike the full process, which takes half an hour or more). Watch both of those. Play with `from/to` in `metadata` until you get it right.

### Create the uploadable video

Run `../../scripts/truncate-video` and go to lunch. Or, since this was already Lunch with a Leader, go to Second Lunch. When you come back you will have `youtube.mp4`. Watch that, skipping around as desired (you don't need to watch the whole thing). Make sure it's OK. If you added subtitles partway through, check at least some of them.

### Upload

Upload to the LWV Youtube channel. Upload `titlepage.png` to use as cover thumbnail. Copy/paste `titlepage.txt` to use as the video title. Copy/paste `blurb` to the big text box.

For Playlist, use `LWL`. Set recording date and location. Click Next. I think the next two screens are useless, just click Next. The last one, set to Publish, and MAKE SURE YOU COPY/SAVE THE LINK!

Wait many hours for upload.
