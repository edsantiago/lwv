# lwv

Tools related to Los Alamos League of Women Voters, e.g., video processing and other needs as they may arise

------------

## titlepage-lwl

Generates a 1920x1080 PNG image suitable for use as an introduction
slide for a Lunch with a Leader video.
